
<section class="content-section my-5 py-5">
    <div class="container">
        <header class="text-center mb-5">
	  <h1>Pengumuman </h1>
	  <p>Pengumuman dan Kebijakan Terbaru</p>
        </header>
        <div class="row">
	  <?php
	  for ($i=1;$i<=5;$i++){
	      ?>
	      <div class="col-md-3">
		<div class="card" style="width: 100%;margin: 10px;box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;">
		    <img class="card-img-top" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22286%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20286%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_17e765e1fe1%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_17e765e1fe1%22%3E%3Crect%20width%3D%22286%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22107.1875%22%20y%3D%2296.3%22%3E286x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image cap">
		    <div class="card-body">
		        <h5 class="card-title">Card title</h5>
		        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		        <a href="#" class="btn btn-primary">Go somewhere</a>
		    </div>
		</div>
	      </div>
	      <?php
	  }
	  ?>

        </div>
        <br>
        <nav aria-label="..." style="float: right">
	  <ul class="pagination"; style="color: black">
	      <li class="page-item disabled">
		<a class="page-link" href="#" style="color: black !important;" tabindex="-1">Previous</a>
	      </li>
	      <li class="page-item"><a class="page-link" href="#"  style="color: black !important;">1</a></li>
	      <li class="page-item active">
		<a class="page-link" href="#" >2 <span class="sr-only" >(current)</span></a>
	      </li>
	      <li class="page-item"><a class="page-link"  style="color: black !important;" href="#">3</a></li>
	      <li class="page-item">
		<a class="page-link"  style="color: black !important;" href="#">Next</a>
	      </li>
	  </ul>
        </nav>
    </div>
</section>

<section class=" my-5 py-5" id="faq">
    <div class="container">
        <div class="row">
	  <div class="col-md-6">
	      <a class="mb-1" style="font-size: 40px!important;color: #0B2C58 !important;font-weight: bold;border-bottom: 6px #FFC107 solid;font-size: 20px;">FAQ</a>
	      <ul class="dropdown-list my-5">
		<li>
		    <div class="dropdown-list__item">
		        <div class="dropdown-list__header d-flex align-items-center justify-content-between" onclick="dropdownClickHandler(event)">
			  <p class="mb-0">{{$faq->question}}</p>
			  <svg class="active" id="down-icon" width="18" height="11" viewBox="0 0 18 11" fill="none" xmlns="http://www.w3.org/2000/svg">
			      <path fill-rule="evenodd" clip-rule="evenodd" d="M17.3968 3.5107C18.201 2.708 18.2012 1.42554 17.3967 0.622783L17.3968 3.5107ZM17.3968 3.5107L10.5171 10.3775C10.1074 10.7862 9.55776 11 9.00011 11C8.44213 11 7.89244 10.7861 7.48294 10.3774L0.603489 3.51069C-0.200903 2.70802 -0.201368 1.42535 0.603321 0.622605C1.43501 -0.207514 2.806 -0.207561 3.6377 0.622615L9.00007 5.97504L14.3622 0.622794C15.194 -0.207036 16.5649 -0.207299 17.3967 0.622783" fill="#39367B"/>
			  </svg>
			  <svg id="up-icon" width="18" height="11" viewBox="0 0 18 11" fill="none" xmlns="http://www.w3.org/2000/svg">
			      <path fill-rule="evenodd" clip-rule="evenodd" d="M0.603247 7.4893C-0.200963 8.292 -0.201203 9.57446 0.603252 10.3772L0.603247 7.4893ZM0.603247 7.4893L7.48292 0.622487C7.8926 0.213773 8.44224 -8.53002e-07 8.99989 -8.46352e-07C9.55787 -8.39698e-07 10.1076 0.213942 10.5171 0.622602L17.3965 7.48931C18.2009 8.29198 18.2014 9.57465 17.3967 10.3774C16.565 11.2075 15.194 11.2076 14.3623 10.3774L8.99993 5.02496L3.63777 10.3772C2.80604 11.207 1.43505 11.2073 0.603252 10.3772" fill="#39367B"/>
			  </svg>

		        </div>
		        <p class="dropdown-list__content mt-2 mb-0">{{$faq->answer}}</p>
		    </div>

		    <div class="dropdown-list__item">
		        <div class="dropdown-list__header d-flex align-items-center justify-content-between" onclick="dropdownClickHandler(event)">
			  <p class="mb-0">{{$faq->question}}</p>
			  <svg class="active" id="down-icon" width="18" height="11" viewBox="0 0 18 11" fill="none" xmlns="http://www.w3.org/2000/svg">
			      <path fill-rule="evenodd" clip-rule="evenodd" d="M17.3968 3.5107C18.201 2.708 18.2012 1.42554 17.3967 0.622783L17.3968 3.5107ZM17.3968 3.5107L10.5171 10.3775C10.1074 10.7862 9.55776 11 9.00011 11C8.44213 11 7.89244 10.7861 7.48294 10.3774L0.603489 3.51069C-0.200903 2.70802 -0.201368 1.42535 0.603321 0.622605C1.43501 -0.207514 2.806 -0.207561 3.6377 0.622615L9.00007 5.97504L14.3622 0.622794C15.194 -0.207036 16.5649 -0.207299 17.3967 0.622783" fill="#39367B"/>
			  </svg>
			  <svg id="up-icon" width="18" height="11" viewBox="0 0 18 11" fill="none" xmlns="http://www.w3.org/2000/svg">
			      <path fill-rule="evenodd" clip-rule="evenodd" d="M0.603247 7.4893C-0.200963 8.292 -0.201203 9.57446 0.603252 10.3772L0.603247 7.4893ZM0.603247 7.4893L7.48292 0.622487C7.8926 0.213773 8.44224 -8.53002e-07 8.99989 -8.46352e-07C9.55787 -8.39698e-07 10.1076 0.213942 10.5171 0.622602L17.3965 7.48931C18.2009 8.29198 18.2014 9.57465 17.3967 10.3774C16.565 11.2075 15.194 11.2076 14.3623 10.3774L8.99993 5.02496L3.63777 10.3772C2.80604 11.207 1.43505 11.2073 0.603252 10.3772" fill="#39367B"/>
			  </svg>

		        </div>
		        <p class="dropdown-list__content mt-2 mb-0">{{$faq->answer}}</p>
		    </div>
		</li>
	      </ul>
	  </div>
	  <div class="col-md-6">
	      <img class="w-100 text-center" src="assets/images/depan2.png" alt="illustration">


	  </div>
        </div>
    </div>
</section>
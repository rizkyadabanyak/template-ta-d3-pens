
<div class="container" style="background-color: #0B2C58;max-width: 100% !important;">
    <nav class="navbar navbar-expand-lg" style="background-color: #0B2C58 !important;">
        <a class="header__logo navbar-brand mr-4" href="/">Sipenda.</a>
    </nav>
</div>

<script src="assets/js/dropdown-list.js"></script>
<script>
    var sentTime = '2022-01-20 23:59:59';
    var deadline = Math.floor(new Date(sentTime).getTime() / 1000);
    var now = Math.floor(Date.now() / 1000);
    var seconds = deadline - now;

    var daysElement = document.querySelector('.countdown__time--days p');
    var hoursElement = document.querySelector('.countdown__time--hours p');
    var minutesElement = document.querySelector('.countdown__time--minutes p');
    var secondsElement = document.querySelector('.countdown__time--seconds p');

    if (seconds > 0) {
        var d = Math.floor(seconds / (3600*24));
        var h = Math.floor(seconds % (3600*24) / 3600);
        var m = Math.floor(seconds % 3600 / 60);
        var s = Math.floor(seconds % 60);

        function setTimeElement() {
            var setSecond = s < 10 ? '0' + s : s;
            var setMinute = m < 10 ? '0' + m : m;
            var setHour = h < 10 ? '0' + h : h;
            var setDay = d < 10 ? '0' + d : d;

            daysElement.innerHTML = setDay;
            hoursElement.innerHTML = setHour;
            minutesElement.innerHTML = setMinute;
            secondsElement.innerHTML = setSecond;
        }

        setTimeElement();

        var timer = setInterval(function() {
            s--;

            if (s < 0) {
                s = 59;
                m--;
                if (m < 0) {
                    m = 59;
                    h--;
                    if (h < 0) {
                        h = 30;
                        d--;
                        if (d < 0) {
                            clearInterval(timer);
                        }
                    }
                }
            }

            setTimeElement();
        }, 1000);
    } else {
        daysElement.innerHTML = '00';
        hoursElement.innerHTML = '00';
        minutesElement.innerHTML = '00';
        secondsElement.innerHTML = '00';
    }
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>